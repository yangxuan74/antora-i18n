(function() {
  var input = document.getElementById ("search-input")
  var searchText = i18n.gettext('Search the docs')
  input.placeholder = searchText
})();

var brand = document.getElementsByClassName("navbar-brand")[0]
var items = brand.getElementsByClassName("navbar-item");
for (let i = 0; i < items.length; i++) {
    var title = i18n.gettext (items[i].innerHTML)
    items[i].innerHTML = title
  }

  var site = document.getElementById("site-lang")
  var doc = document.getElementById("page-lang")
  site.title = i18n.gettext('Site language')

  if(document.getElementById('page-lang') != null){
      doc.title = i18n.gettext('Document language')
    }

  var edit = document.getElementsByClassName("edit-this-page")[0]
  if (document.getElementsByClassName("edit-this-page").length > 0) {
      edit.innerHTML = i18n.gettext('Edit this page')
    }

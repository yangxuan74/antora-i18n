#!/usr/bin/bash
l10n=./l10n

po4a --no-translations --variable langs="fr dyu" po4a.cfg
git add -f $l10n/**.po
git commit $l10n/* -m "updated by $0 on: $(date)"
